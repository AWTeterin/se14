package ru.teterin.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.entity.ProjectPOJO;
import ru.teterin.tm.entity.TaskPOJO;
import ru.teterin.tm.entity.UserPOJO;
import ru.teterin.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractDTO implements Serializable {

    public static final long serialVersionUID = 2;

    @NotNull
    private String userId = "";

    @NotNull
    private String projectId = "";

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateEnd = new Date();

    @NotNull
    private Status status = Status.PLANNED;

    @Nullable
    public TaskPOJO toTaskPOJO(
        @NotNull final IServiceLocator serviceLocator
    ) throws Exception {
        @NotNull final TaskPOJO taskPOJO = new TaskPOJO();
        @Nullable final UserPOJO userPOJO = serviceLocator.getUserService().findOne(userId);
        if (userPOJO == null) return null;
        @Nullable final ProjectPOJO projectPOJO = serviceLocator.getProjectService().findOne(userId, projectId);
        if (projectPOJO == null) return null;
        taskPOJO.setId(id);
        taskPOJO.setUser(userPOJO);
        taskPOJO.setProject(projectPOJO);
        taskPOJO.setName(name);
        taskPOJO.setDescription(description);
        taskPOJO.setDateCreate(dateCreate);
        taskPOJO.setDateStart(dateStart);
        taskPOJO.setDateEnd(dateEnd);
        taskPOJO.setStatus(status);
        return taskPOJO;
    }

}
