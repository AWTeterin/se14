package ru.teterin.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.entity.ProjectPOJO;
import ru.teterin.tm.entity.TaskPOJO;
import ru.teterin.tm.entity.UserPOJO;
import ru.teterin.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractDTO implements Serializable {

    public static final long serialVersionUID = 1;

    @NotNull
    private String userId = "";

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateEnd = new Date();

    @NotNull
    private Status status = Status.PLANNED;

    @Nullable
    public ProjectPOJO toProjectPOJO(
        @NotNull final IServiceLocator serviceLocator
    ) throws Exception {
        @NotNull final ProjectPOJO projectPOJO = new ProjectPOJO();
        @Nullable final UserPOJO userPOJO = serviceLocator.getUserService().findOne(userId);
        if (userPOJO == null) return null;
        @NotNull final List<TaskPOJO> taskList = serviceLocator.getProjectService()
            .findAllTaskByProjectId(userId, id);
        projectPOJO.setId(id);
        projectPOJO.setUser(userPOJO);
        projectPOJO.setTasks(taskList);
        projectPOJO.setName(name);
        projectPOJO.setDescription(description);
        projectPOJO.setDateCreate(dateCreate);
        projectPOJO.setDateStart(dateStart);
        projectPOJO.setDateEnd(dateEnd);
        projectPOJO.setStatus(status);
        return projectPOJO;
    }

}
