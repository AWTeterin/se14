package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.IProjectEndpoint;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.dto.Project;
import ru.teterin.tm.dto.Task;
import ru.teterin.tm.entity.ProjectPOJO;
import ru.teterin.tm.entity.TaskPOJO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @Override
    @WebMethod
    public void mergeProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        projectService.merge(userId, project.toProjectPOJO(serviceLocator));
    }

    @Override
    @WebMethod
    public void persistProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        project.setUserId(userId);
        ProjectPOJO result = project.toProjectPOJO(serviceLocator);
        projectService.persist(result);
    }

    @NotNull
    @Override
    @WebMethod
    public Project findOneProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final Project project = projectService.findOne(userId, projectId).toProject();
        return project;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProject(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final List<ProjectPOJO> projects = projectService.findAll(userId);
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final ProjectPOJO project : projects) {
            result.add(project.toProject());
        }
        return result;
    }

    @Override
    @WebMethod
    public void removeProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        projectService.remove(userId, projectId);
    }

    @Override
    @WebMethod
    public void removeAllProjects(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        projectService.removeAll(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> searchProjectByString(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final List<ProjectPOJO> projects = projectService.searchByString(userId, string);
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final ProjectPOJO project : projects) {
            result.add(project.toProject());
        }
        return result;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> sortAllProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final List<ProjectPOJO> projects = projectService.findAndSortAll(userId, sortOption);
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final ProjectPOJO project : projects) {
            result.add(project.toProject());
        }
        return result;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllProjectTasks(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final List<TaskPOJO> tasks = projectService.findAllTaskByProjectId(userId, projectId);
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final TaskPOJO task : tasks) {
            result.add(task.toTask());
        }
        return result;
    }

}
