package ru.teterin.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.ISqlSessionService;

import javax.sql.DataSource;

public class SqlSessionService implements ISqlSessionService {

    @NotNull
    final IServiceLocator serviceLocator;

    public SqlSessionService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String user = serviceLocator.getPropertyService().getJdbcLogin();
        @Nullable final String password = serviceLocator.getPropertyService().getJdbcPassword();
        @Nullable final String url = serviceLocator.getPropertyService().getJdbcUrl();
        @Nullable final String driver = serviceLocator.getPropertyService().getJdbcDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
