package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.Application;
import ru.teterin.tm.api.service.IPropertyService;
import ru.teterin.tm.constant.Constant;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        try (
            @Nullable final InputStream inputStream = Application.class.getClassLoader().getResourceAsStream(Constant.PROPERTIES_FILE)
        ) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        @NotNull final String cycle = properties.getProperty("session.cycle");
        boolean noDigit = cycle == null || !cycle.matches("\\d+");
        if (noDigit) {
            return 0;
        }
        return Integer.parseInt(cycle);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return properties.getProperty("server.host");
    }

    @NotNull
    @Override
    public String getServerPort() {
        return properties.getProperty("server.port");
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return properties.getProperty("jdbc.driver");
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return properties.getProperty("jdbc.url");
    }

    @NotNull
    @Override
    public String getJdbcLogin() {
        return properties.getProperty("jdbc.login");
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return properties.getProperty("jdbc.password");
    }

    @NotNull
    @Override
    public String getSecretKey() {
        return properties.getProperty("token.secretKey");
    }

}
