package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.UserPOJO;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public UserRepository(
        @NotNull final EntityManager entityManager
    ) {
        this.entityManager = entityManager;
    }

    @Nullable
    public UserPOJO findOne(
        @NotNull final String id
    ) throws Exception {
        @NotNull final List<UserPOJO> users = entityManager.createQuery("SELECT u FROM UserPOJO u WHERE u.id = :id", UserPOJO.class)
            .setParameter("id", id).getResultList();
        if (users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }

    @NotNull
    public List<UserPOJO> findAll() {
        return entityManager.createQuery("SELECT u FROM UserPOJO u", UserPOJO.class).getResultList();
    }

    public void persist(
        @NotNull final UserPOJO userPOJO
    ) {
        entityManager.persist(userPOJO);
    }

    public void merge(
        @NotNull final UserPOJO userPOJO
    ) {
        entityManager.merge(userPOJO);
    }

    public void remove(
        @NotNull final String id
    ) throws Exception {
        @Nullable final UserPOJO user = findOne(id);
        if (user == null) {
            entityManager.close();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        }
        entityManager.remove(user);
    }

    public void removeAll() {
        @NotNull final List<UserPOJO> users = findAll();
        for (@NotNull final UserPOJO user : users) {
            entityManager.remove(user);
        }
    }

    @Nullable
    public UserPOJO findByLogin(
        @NotNull final String login
    ) {
        @NotNull final List<UserPOJO> users = entityManager.createQuery("SELECT u FROM UserPOJO u WHERE u.login = :login", UserPOJO.class)
            .setParameter("login", login).getResultList();
        if (users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }

    public boolean loginIsFree(
        @NotNull final String login
    ) {
        return entityManager.createQuery("SELECT u FROM UserPOJO u WHERE u.login = :login")
            .setParameter("login", login).getResultList().isEmpty();
    }

}
