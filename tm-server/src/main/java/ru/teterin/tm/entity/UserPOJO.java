package ru.teterin.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.dto.User;
import ru.teterin.tm.enumerated.Role;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "app_user")
public final class UserPOJO extends AbstractPOJO {

    @Nullable
    @JsonIgnore
    @OneToOne(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private SessionPOJO session;

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<ProjectPOJO> projects;

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<TaskPOJO> tasks;

    @NotNull
    @Column(unique = true)
    private String login = "";

    @NotNull
    private String password = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @NotNull
    public User toUser() {
        @NotNull final User user = new User();
        user.setId(id);
        user.setLogin(login);
        user.setPassword(password);
        user.setRole(role);
        return user;
    }

}
