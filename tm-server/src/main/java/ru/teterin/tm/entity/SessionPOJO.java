package ru.teterin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.enumerated.Role;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "app_session")
public final class SessionPOJO extends AbstractPOJO {

    @NotNull
    @OneToOne
    @JoinColumn(name = "userId")
    private UserPOJO user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

    private long timestamp = System.currentTimeMillis();

    @Nullable
    private String signature;

    @NotNull
    public SessionPOJO clone() {
        @NotNull SessionPOJO session = new SessionPOJO();
        session.setId(id);
        session.setUser(user);
        session.setRole(role);
        session.setTimestamp(timestamp);
        session.setSignature(signature);
        return session;
    }

}
