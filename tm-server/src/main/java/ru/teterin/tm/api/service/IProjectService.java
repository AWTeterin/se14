package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.ProjectPOJO;
import ru.teterin.tm.entity.TaskPOJO;

import java.util.List;

public interface IProjectService {

    @NotNull
    public ProjectPOJO findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public List<ProjectPOJO> findAll();

    @NotNull
    public List<ProjectPOJO> findAll(
        @Nullable final String userId
    ) throws Exception;

    public void persist(
        @Nullable final ProjectPOJO project
    ) throws Exception;

    public void merge(
        @Nullable final String userId,
        @Nullable final ProjectPOJO project
    ) throws Exception;

    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception;

    public void removeAll();

    public void removeAll(
        @Nullable final String userId
    ) throws Exception;

    @NotNull
    public List<TaskPOJO> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public List<ProjectPOJO> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception;

    @NotNull
    public List<ProjectPOJO> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception;

}
