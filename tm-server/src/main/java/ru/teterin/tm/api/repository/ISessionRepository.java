package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.SessionPOJO;

import java.util.List;

public interface ISessionRepository {

    @Nullable
    public SessionPOJO findOne(
        @NotNull final String id
    );

    @NotNull
    public List<SessionPOJO> findAll();

    public void persist(
        @NotNull final SessionPOJO session
    );

    public void remove(
        @NotNull final String id
    ) throws Exception;

    public void removeAll();

    public boolean contains(
        @NotNull final String id
    );

}
