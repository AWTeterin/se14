package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.UserPOJO;

import java.util.List;

public interface IUserRepository {

    @Nullable
    public UserPOJO findOne(
        @NotNull final String id
    ) throws Exception;

    @NotNull
    public List<UserPOJO> findAll() throws Exception;

    public void persist(
        @NotNull final UserPOJO user
    ) throws Exception;

    public void merge(
        @NotNull final UserPOJO user
    ) throws Exception;

    public void remove(
        @NotNull final String id
    ) throws Exception;

    public void removeAll() throws Exception;

    @Nullable
    public UserPOJO findByLogin(
        @NotNull final String login
    ) throws Exception;

    public boolean loginIsFree(
        @NotNull final String login
    );

}
