package ru.teterin.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class SqlConnectionUtil {

    @NotNull
    private static final PropertyService propertyService = new PropertyService();

    @NotNull
    private static final String driver = propertyService.getJdbcDriver();

    @NotNull
    private static final String url = propertyService.getJdbcUrl();

    @NotNull
    private static final String login = propertyService.getJdbcLogin();

    @NotNull
    private static final String password = propertyService.getJdbcPassword();

    public static Connection connect() throws Exception {
        Class.forName(driver);
        @NotNull final Connection connection = DriverManager.getConnection(url, login, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
