package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.api.endpoint.Project;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import java.util.Collection;

public final class ProjectSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for a string in the project name or description.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.PROJECT_SEARCH);
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_SEARCH_STRING);
        @Nullable final String searchString = terminalService.readString();
        projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final Collection<Project> projects = projectEndpoint.searchProjectByString(session, searchString);
        terminalService.printCollection(projects);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
