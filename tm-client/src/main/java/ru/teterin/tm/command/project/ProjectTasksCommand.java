package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.api.endpoint.Task;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import java.util.Collection;

public final class ProjectTasksCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-tasks";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks for this project.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.PROJECT_TASKS);
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_PROJECT_ID);
        @Nullable final String id = terminalService.readString();
        projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final Collection<Task> tasks = projectEndpoint.findAllProjectTasks(session, id);
        terminalService.printCollection(tasks);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
