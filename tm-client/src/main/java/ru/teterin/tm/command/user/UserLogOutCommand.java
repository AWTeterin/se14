package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Log out to the task manager.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.USER_LOGOUT);
        @Nullable final String session = stateService.getSession();
        if (session != null) {
            sessionEndpoint = serviceLocator.getSessionEndpoint();
            sessionEndpoint.closeSession(session);
        }
        stateService.setSession(null);
        stateService.setLogin(null);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
