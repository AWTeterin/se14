package ru.teterin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import ru.teterin.tm.api.endpoint.*;
import ru.teterin.tm.endpoint.ProjectEndpointService;
import ru.teterin.tm.endpoint.SessionEndpointService;
import ru.teterin.tm.endpoint.TaskEndpointService;
import ru.teterin.tm.endpoint.UserEndpointService;
import ru.teterin.tm.util.DateUuidParseUtil;
import ru.teterin.tm.util.PasswordHashUtil;

import java.lang.Exception;
import java.util.UUID;

public abstract class AbstractTest {

    @NotNull
    protected static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    protected static final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    protected static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    protected static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    protected static String adminToken;

    @Nullable
    protected static String adminId;

    @Nullable
    protected static User user;

    @Nullable
    protected static String userId;

    @Nullable
    protected static Project project1;

    @Nullable
    protected static String project1Id;

    @Nullable
    protected static Project project2;

    @Nullable
    protected static String project2Id;

    @Nullable
    protected static Task task1;

    @Nullable
    protected static String task1Id;

    @Nullable
    protected static Task task2;

    @Nullable
    protected static String task2Id;

    @Nullable
    protected static Task task3;

    @Nullable
    protected static String task3Id;

    @BeforeClass
    public static void setting() throws Exception {
        initUsers();
        initProjects();
        initTask();
    }

    public static void initUsers() throws Exception {
        user = new User();
        user.setId(UUID.randomUUID().toString());
        userId = user.getId();
        user.setLogin("testuser");
        @NotNull String userPassword = "testuser";
        userPassword = PasswordHashUtil.md5(userPassword);
        user.setPassword(userPassword);
        user.setRole(Role.USER);
        @NotNull final User admin = new User();
        admin.setId(UUID.randomUUID().toString());
        adminId = admin.getId();
        admin.setLogin("testadmin");
        @NotNull String adminPassword = "testadmin";
        adminPassword = PasswordHashUtil.md5(adminPassword);
        admin.setPassword(adminPassword);
        admin.setRole(Role.ADMIN);
        userEndpoint.persistUser(admin);
        adminToken = sessionEndpoint.openSession(admin.getLogin(), adminPassword);
    }

    public static void initProjects() {
        project1 = new Project();
        project1Id = UUID.randomUUID().toString();
        project1.setId(project1Id);
        project1.setUserId(adminId);
        project1.setName("project1searchstring");
        project1.setDescription("testproject1");
        project1.setDateCreate(DateUuidParseUtil.isDate("01.01.2020"));
        project1.setDateStart(DateUuidParseUtil.isDate("01.01.2020"));
        project1.setDateEnd(DateUuidParseUtil.isDate("01.03.2020"));
        project1.setStatus(Status.PLANNED);
        project2 = new Project();
        project2Id = UUID.randomUUID().toString();
        project2.setId(project2Id);
        project2.setUserId(adminId);
        project2.setName("project2");
        project2.setDescription("test searchstring project2");
        project2.setDateCreate(DateUuidParseUtil.isDate("01.02.2020"));
        project2.setDateStart(DateUuidParseUtil.isDate("01.02.2020"));
        project2.setDateEnd(DateUuidParseUtil.isDate("01.02.2020"));
        project2.setStatus(Status.IN_PROGRESS);
    }

    public static void initTask() {
        task1 = new Task();
        task1Id = UUID.randomUUID().toString();
        task1.setId(task1Id);
        task1.setUserId(adminId);
        task1.setProjectId(project1Id);
        task1.setName("task1searchstring");
        task1.setDescription("testtask1");
        task1.setDateCreate(DateUuidParseUtil.isDate("01.01.2020"));
        task1.setDateStart(DateUuidParseUtil.isDate("01.01.2020"));
        task1.setDateEnd(DateUuidParseUtil.isDate("01.03.2020"));
        task1.setStatus(Status.PLANNED);
        task2 = new Task();
        task2Id = UUID.randomUUID().toString();
        task2.setId(task2Id);
        task2.setUserId(adminId);
        task2.setName("task2");
        task2.setProjectId(project1Id);
        task2.setDescription("test searchstring task2");
        task2.setDateCreate(DateUuidParseUtil.isDate("01.02.2020"));
        task2.setDateStart(DateUuidParseUtil.isDate("01.02.2020"));
        task2.setDateEnd(DateUuidParseUtil.isDate("01.02.2020"));
        task2.setStatus(Status.IN_PROGRESS);
        task3 = new Task();
        task3Id = UUID.randomUUID().toString();
        task3.setId(task3Id);
        task3.setUserId(adminId);
        task3.setProjectId(project2Id);
        task3.setName("task3");
        task3.setDescription("task3");
        task3.setDateCreate(DateUuidParseUtil.isDate("20.01.2020"));
        task3.setDateStart(DateUuidParseUtil.isDate("20.01.2020"));
        task3.setDateEnd(DateUuidParseUtil.isDate("20.02.2020"));
        task3.setStatus(Status.READY);
    }

    // Запись в бд 2 проектов.
    protected static void saveProject() throws Exception {
        projectEndpoint.mergeProject(adminToken, project1);
        projectEndpoint.mergeProject(adminToken, project2);
    }

    //Запись 2 проектов и 3 задач. Первая и вторая задача первого проекта, и третья задача второго проекта.
    protected static void saveProjectAndTask() throws Exception {
        saveProject();
        taskEndpoint.mergeTask(adminToken, task1);
        taskEndpoint.mergeTask(adminToken, task2);
        taskEndpoint.mergeTask(adminToken, task3);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        projectEndpoint.removeAllProjects(adminToken);
        userEndpoint.removeUser(adminToken, adminId);
    }

}
