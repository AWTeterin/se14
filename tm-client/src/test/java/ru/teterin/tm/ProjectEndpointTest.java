package ru.teterin.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.teterin.tm.api.endpoint.Project;
import ru.teterin.tm.api.endpoint.Task;

import java.util.List;

public final class ProjectEndpointTest extends AbstractTest {

    @Test
    public void findOneProject() throws Exception {
        projectEndpoint.persistProject(adminToken, project1);
        @NotNull final Project project = projectEndpoint.findOneProject(adminToken, project1Id);
        Assert.assertNotNull(project);
        Assert.assertEquals(project, project1);
        projectEndpoint.removeProject(adminToken, project1Id);
    }

    @Test
    public void findAllProject() throws Exception {
        saveProject();
        @NotNull final List<Project> projects = projectEndpoint.findAllProject(adminToken);
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.contains(project1));
        Assert.assertTrue(projects.contains(project2));
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void mergeProject() throws Exception {
        projectEndpoint.persistProject(adminToken, project1);
        @NotNull final Project project = projectEndpoint.findOneProject(adminToken, project1Id);
        Assert.assertEquals(project, project1);
        project.setName("newName");
        project.setDescription("newDescription");
        projectEndpoint.mergeProject(adminToken, project);
        @NotNull final Project result = projectEndpoint.findOneProject(adminToken, project1Id);
        Assert.assertEquals(result, project);
        projectEndpoint.removeProject(adminToken, project1Id);
    }

    @Test
    public void persistProject() throws Exception {
        projectEndpoint.persistProject(adminToken, project1);
        @NotNull final Project project = projectEndpoint.findOneProject(adminToken, project1Id);
        Assert.assertEquals(project1, project);
        projectEndpoint.removeProject(adminToken, project1Id);
    }

    @Test
    public void removeProject() throws Exception {
        projectEndpoint.persistProject(adminToken, project1);
        @NotNull final Project project = projectEndpoint.findOneProject(adminToken, project1Id);
        Assert.assertEquals(project, project1);
        projectEndpoint.removeProject(adminToken, project1Id);
        try {
            projectEndpoint.findOneProject(adminToken, project1Id);
            Assert.fail("Excepted ObjectNotFoundException");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void removeAllProject() throws Exception {
        saveProject();
        projectEndpoint.removeAllProjects(adminToken);
        @NotNull final List<Project> projects = projectEndpoint.findAllProject(adminToken);
        Assert.assertTrue(projects.isEmpty());
    }

    @Test
    public void searchProjectByString() throws Exception {
        saveProject();
        @NotNull List<Project> projects = projectEndpoint.searchProjectByString(adminToken, "search");
        Assert.assertTrue(projects.contains(project1));
        Assert.assertTrue(projects.contains(project2));
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void sortAllProject() throws Exception {
        saveProject();
        @NotNull List<Project> projects = projectEndpoint.sortAllProject(adminToken, "11");
        Assert.assertEquals(projects.get(0), project1);
        Assert.assertEquals(projects.get(1), project2);
        projects = projectEndpoint.sortAllProject(adminToken, "21");
        Assert.assertEquals(projects.get(0), project1);
        Assert.assertEquals(projects.get(1), project2);
        projects = projectEndpoint.sortAllProject(adminToken, "31");
        Assert.assertEquals(projects.get(0), project2);
        Assert.assertEquals(projects.get(1), project1);
        projects = projectEndpoint.sortAllProject(adminToken, "41");
        Assert.assertEquals(projects.get(0), project2);
        Assert.assertEquals(projects.get(1), project1);
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void findAllProjectTasks() throws Exception {
        saveProjectAndTask();
        @NotNull List<Task> tasks = projectEndpoint.findAllProjectTasks(adminToken, project1Id);
        Assert.assertTrue(tasks.contains(task1));
        Assert.assertTrue(tasks.contains(task2));
        Assert.assertFalse(tasks.contains(task3));
        tasks = projectEndpoint.findAllProjectTasks(adminToken, project2Id);
        Assert.assertTrue(tasks.contains(task3));
        Assert.assertFalse(tasks.contains(task1));
        Assert.assertFalse(tasks.contains(task2));
        projectEndpoint.removeAllProjects(adminToken);
    }

}
