package ru.teterin.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.teterin.tm.api.endpoint.Task;

import java.util.List;

public final class TaskEndpointTest extends AbstractTest {

    @Test
    public void mergeTask() throws Exception {
        saveProjectAndTask();
        @NotNull final Task task = taskEndpoint.findOneTask(adminToken, task1Id);
        Assert.assertNotNull(task);
        task.setName("newName");
        task.setDescription("newDescription");
        taskEndpoint.mergeTask(adminToken, task);
        @NotNull final Task result = taskEndpoint.findOneTask(adminToken, task1Id);
        Assert.assertNotNull(result);
        Assert.assertEquals(result, task);
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void persistTask() throws Exception {
        saveProject();
        taskEndpoint.persistTask(adminToken, task1);
        @NotNull final Task task = taskEndpoint.findOneTask(adminToken, task1Id);
        Assert.assertEquals(task, task1);
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void findOneTask() throws Exception {
        saveProjectAndTask();
        @NotNull final Task task = taskEndpoint.findOneTask(adminToken, task1Id);
        Assert.assertEquals(task, task);
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void findAllTask() throws Exception {
        saveProjectAndTask();
        @NotNull final List<Task> tasks = taskEndpoint.findAllTask(adminToken);
        Assert.assertTrue(tasks.contains(task1));
        Assert.assertTrue(tasks.contains(task2));
        Assert.assertTrue(tasks.contains(task3));
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void removeTask() throws Exception {
        saveProjectAndTask();
        taskEndpoint.removeTask(adminToken, task1Id);
        try {
            taskEndpoint.findOneTask(adminToken, task1Id);
            Assert.fail("Excepted ObjectNotFoundException");
        } catch (Exception ignore) {
        }
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void removeAllTask() throws Exception {
        saveProjectAndTask();
        taskEndpoint.removeAllTasks(adminToken);
        @NotNull final List<Task> tasks = taskEndpoint.findAllTask(adminToken);
        Assert.assertTrue(tasks.isEmpty());
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void searchTaskByString() throws Exception {
        saveProjectAndTask();
        @NotNull final List<Task> tasks = taskEndpoint.searchTaskByString(adminToken, "search");
        Assert.assertTrue(tasks.contains(task1));
        Assert.assertTrue(tasks.contains(task2));
        Assert.assertFalse(tasks.contains(task3));
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void sortAllTask() throws Exception {
        saveProjectAndTask();
        @NotNull List<Task> tasks = taskEndpoint.sortAllTask(adminToken, "11");
        Assert.assertEquals(tasks.get(0), task1);
        Assert.assertEquals(tasks.get(1), task3);
        Assert.assertEquals(tasks.get(2), task2);
        tasks = taskEndpoint.sortAllTask(adminToken, "21");
        Assert.assertEquals(tasks.get(0), task1);
        Assert.assertEquals(tasks.get(1), task3);
        Assert.assertEquals(tasks.get(2), task2);
        tasks = taskEndpoint.sortAllTask(adminToken, "31");
        Assert.assertEquals(tasks.get(0), task2);
        Assert.assertEquals(tasks.get(1), task3);
        Assert.assertEquals(tasks.get(2), task1);
        tasks = taskEndpoint.sortAllTask(adminToken, "41");
        Assert.assertEquals(tasks.get(0), task2);
        Assert.assertEquals(tasks.get(1), task1);
        Assert.assertEquals(tasks.get(2), task3);
        projectEndpoint.removeAllProjects(adminToken);
    }

    @Test
    public void linkTask() throws Exception {
        saveProjectAndTask();
        taskEndpoint.linkTask(adminToken, project1Id, task3Id);
        @NotNull final Task task = taskEndpoint.findOneTask(adminToken, task3Id);
        Assert.assertEquals(task.getProjectId(), project1Id);
        projectEndpoint.removeAllProjects(adminToken);
    }

}
